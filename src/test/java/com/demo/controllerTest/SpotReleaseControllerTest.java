package com.demo.controllerTest;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.employeeparking.dto.RequestResponseDto;
import com.example.employeeparking.model.Employee;
import com.example.employeeparking.service.SpotReleaseService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SpotReleaseControllerTest {

	@InjectMocks
	SpotReleaseController spotReleaseController;

	@Mock
	SpotReleaseService spotReleaseService;

	@Test
	public void TestReleaseSpotForPositive() {

		RequestResponseDto requestResponseDtos = new RequestResponseDto();
		requestResponseDtos.setMessage("success");

		Mockito.when(spotReleaseService.releaseSpot(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenReturn(requestResponseDtos);

		ResponseEntity<RequestResponseDto> result = spotReleaseController.releaseSpot(Mockito.anyLong(), Mockito.any(),
				Mockito.any());
		assertNotNull(result);

	}

	@Test
	public void TestReleaseSpotForNegative() {

		RequestResponseDto requestResponseDtos = new RequestResponseDto();
		requestResponseDtos.setMessage("success");

		Employee employee = new Employee();
		employee.setEmail("dhay@gmaol.com");
		employee.setEmployeeId(-1L);
		employee.setEmployeeName("dhayanamajoa");
		employee.setIsVip(true);
		employee.setPassword("Dhay8870");
		employee.setPhone("8870448827");

		Mockito.when(spotReleaseService.releaseSpot(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenReturn(requestResponseDtos);

		ResponseEntity<RequestResponseDto> result = spotReleaseController.releaseSpot(employee.getEmployeeId(),
				LocalDate.of(2020, 06, 18), LocalDate.of(2020, 06, 18));
		assertNotNull(result);

	}

}