package com.demo.controllerTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.employeeparking.dto.RequestResponseDto;
import com.example.employeeparking.exceptions.SpotRequestNotFoundException;
import com.example.employeeparking.model.Employee;
import com.example.employeeparking.model.FreeSpot;
import com.example.employeeparking.model.ParkingLot;
import com.example.employeeparking.model.SpotRequest;
import com.example.employeeparking.service.SpotRequestService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SpotRequestControllerTest {

	@InjectMocks
	SpotRequestController spotRequestController;
	@Mock
	SpotRequestService spotRequestService;

	RequestResponseDto responseDto = new RequestResponseDto();

	@Test
	public void testCreateUserForPositive() throws Exception {

		responseDto.setMessage("sucessful");
		responseDto.getMessage();
		Mockito.when(spotRequestService.requestProcessing()).thenReturn(responseDto);
		ResponseEntity<RequestResponseDto> responseDto1 = spotRequestController.requestProcessingByLotterySystem();
	}

	@Test(expected = NullPointerException.class)
	public void testCreateUserForExce() throws Exception {
		Mockito.when(spotRequestService.requestProcessing()).thenThrow(NullPointerException.class);
		ResponseEntity<RequestResponseDto> responseDto1 = spotRequestController.requestProcessingByLotterySystem();

	}

	@Test
	public void TestGetEmployeeIdForPositive() throws Exception {

		Employee employee = new Employee();
		employee.setEmail("dhay@gmaol.com");
		employee.setEmployeeId(1L);
		employee.setEmployeeName("dhayanamajoa");
		employee.setIsVip(true);
		employee.setPassword("Dhay8870");
		employee.setPhone("8870448827");

		List<SpotRequest> list = new ArrayList<>();
		SpotRequest spots = new SpotRequest();
		spots.setDate(LocalDate.of(2020, 06, 17));
//	         spots.setEmployee(employee);
//	         spots.setFreeSpot(freeSpot);

		spots.setSpotRequesttId(1L);
		spots.setStatus("spot is safety");
		list.add(spots);

		Mockito.when(spotRequestService.getByEmployeeId(Mockito.anyLong())).thenReturn(list);

		ResponseEntity<List<SpotRequest>> result = spotRequestController.getEmployeeById(employee.getEmployeeId());

		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	public void TestGetEmployeeIdForNegative() throws Exception {

		ParkingLot parkingLot = new ParkingLot();
		parkingLot.setDescription("comfortable");
		parkingLot.setParkingLotId(1L);
		parkingLot.setSpot("left wing");

		Employee employee = new Employee();
		employee.setEmail("dhay@gmail.com");
		employee.setEmployeeId(-1L);
		employee.setEmployeeName("dhayanamajoa");
		employee.setIsVip(true);
		employee.setPassword("Dhay8870");
		employee.setPhone("8870448827");

		FreeSpot freeSpot = new FreeSpot();
		freeSpot.setDate(LocalDate.now());
		freeSpot.setEmployee(employee);
		freeSpot.setFreeSpotId(1L);
		freeSpot.setParkingLot(parkingLot);
		freeSpot.setStatus("Assingned");

		List<SpotRequest> list = new ArrayList<>();
		SpotRequest spots = new SpotRequest();
		spots.setDate(LocalDate.of(2020, 06, 17));
		spots.setEmployee(employee);
		spots.setFreeSpot(freeSpot);

		spots.setSpotRequesttId(-1L);
		spots.setStatus("spot is safety");
		list.add(spots);

		Mockito.when(spotRequestService.getByEmployeeId(Mockito.anyLong())).thenReturn(list);

		ResponseEntity<List<SpotRequest>> result = spotRequestController.getEmployeeById(employee.getEmployeeId());

		assertNotEquals(2L, result);

	}

	@Test
	public void TestRaiseRequestForPositive() {

		RequestResponseDto requestResponseDtos = new RequestResponseDto();
		requestResponseDtos.setMessage("success");

		Employee employee = new Employee();
		employee.setEmail("dhay@gmaol.com");
		employee.setEmployeeId(1L);
		employee.setEmployeeName("dhayanamajoa");
		employee.setIsVip(true);
		employee.setPassword("Dhay8870");
		employee.setPhone("8870448827");

		Mockito.when(spotRequestService.raiseRequest(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenReturn(requestResponseDtos);

		ResponseEntity<RequestResponseDto> result = spotRequestController.raiseRequest(employee.getEmployeeId(),
				LocalDate.of(2020, 06, 18), LocalDate.of(2020, 06, 18));
		assertEquals(HttpStatus.OK, result.getStatusCode());

	}

	@Test
	public void TestRaiseRequestForNegative() {

		RequestResponseDto requestResponseDtos = new RequestResponseDto();
		requestResponseDtos.setMessage("success");

		Employee employee = new Employee();
		employee.setEmail("dhay@gmaol.com");
		employee.setEmployeeId(-1L);
		employee.setEmployeeName("dhayanamajoa");
		employee.setIsVip(true);
		employee.setPassword("Dhay8870");
		employee.setPhone("8870448827");

		Mockito.when(spotRequestService.raiseRequest(Mockito.anyLong(), Mockito.any(), Mockito.any()))
				.thenReturn(requestResponseDtos);

		ResponseEntity<RequestResponseDto> result = spotRequestController.raiseRequest(employee.getEmployeeId(),
				LocalDate.of(2020, 06, 18), LocalDate.of(2020, 06, 18));
		assertNotNull(result);

	}

}
