package com.demo.serviceTest;


import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.employeeparking.dto.RequestResponseDto;
import com.example.employeeparking.model.Employee;
import com.example.employeeparking.model.EmployeeParkingLot;
import com.example.employeeparking.model.FreeSpot;
import com.example.employeeparking.model.ParkingLot;
import com.example.employeeparking.repository.EmployeeParkingLotRepository;
import com.example.employeeparking.repository.EmployeeRepository;
import com.example.employeeparking.repository.FreeSpotRepository;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SpotReleaseServiceTest {

	@InjectMocks
	SpotReleaseService spotReleaseService;

	@Mock
	EmployeeRepository employeeRepository;

	@Mock
	FreeSpotRepository freeSpotRepository;

	@Mock
	EmployeeParkingLotRepository employeeParkingLotRepository;

	@Test
	public void TestReleaseSpotForPositive() {

		RequestResponseDto requestResponseDtos = new RequestResponseDto();
		requestResponseDtos.setMessage("success");

		ParkingLot parkingLot = new ParkingLot();
		parkingLot.setDescription("comfortable");
		parkingLot.setParkingLotId(1L);
		parkingLot.setSpot("left wing");

		Employee employee = new Employee();
		employee.setEmail("dhay@gmaol.com");
		employee.setEmployeeId(1L);
		employee.setEmployeeName("dhayanamajoa");
		employee.setIsVip(true);
		employee.setPassword("Dhay8870");
		employee.setPhone("8870448827");

		EmployeeParkingLot employeeParkingLots = new EmployeeParkingLot();
		employeeParkingLots.setEmployee(employee);
		employeeParkingLots.setId(1L);
		employeeParkingLots.setParkingLot(parkingLot);
		employeeParkingLots.setStatus("Assigned");

		FreeSpot freeSpot = new FreeSpot();
		freeSpot.setDate(LocalDate.now());
		freeSpot.setEmployee(employee);
		freeSpot.setFreeSpotId(1L);
		freeSpot.setParkingLot(parkingLot);
		freeSpot.setStatus("Assingned");

		Mockito.when(employeeRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(employee));
		Mockito.when(employeeParkingLotRepository.findByEmployee(Mockito.any())).thenReturn(employeeParkingLots);
		Mockito.when(freeSpotRepository.save(Mockito.any())).thenReturn(Mockito.any());

		RequestResponseDto result = spotReleaseService.releaseSpot(employee.getEmployeeId(), LocalDate.of(2020, 06, 16),
				LocalDate.of(2020, 06, 18));
		assertNotNull(result);

	}
	
	@Test
	public void TestReleaseSpotForNegative() {

		RequestResponseDto requestResponseDtos = new RequestResponseDto();
		requestResponseDtos.setMessage("success");

		ParkingLot parkingLot = new ParkingLot();
		parkingLot.setDescription("comfortable");
		parkingLot.setParkingLotId(-1L);
		parkingLot.setSpot("left wing");

		Employee employee = new Employee();
		employee.setEmail("dhay@gmaol.com");
		employee.setEmployeeId(1L);
		employee.setEmployeeName("dhayanamajoa");
		employee.setIsVip(true);
		employee.setPassword("Dhay8870");
		employee.setPhone("8870448827");

		EmployeeParkingLot employeeParkingLots = new EmployeeParkingLot();
		employeeParkingLots.setEmployee(employee);
		employeeParkingLots.setId(1L);
		employeeParkingLots.setParkingLot(parkingLot);
		employeeParkingLots.setStatus("Assigned");

		FreeSpot freeSpot = new FreeSpot();
		freeSpot.setDate(LocalDate.now());
		freeSpot.setEmployee(employee);
		freeSpot.setFreeSpotId(1L);
		freeSpot.setParkingLot(parkingLot);
		freeSpot.setStatus("Assingned");

		Mockito.when(employeeRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(employee));
		Mockito.when(employeeParkingLotRepository.findByEmployee(Mockito.any())).thenReturn(employeeParkingLots);
		Mockito.when(freeSpotRepository.save(Mockito.any())).thenReturn(Mockito.any());

		RequestResponseDto result = spotReleaseService.releaseSpot(employee.getEmployeeId(), LocalDate.of(2020, 06, 16),
				LocalDate.of(2020, 06, 18));
		assertNotNull(result);

	}

}
