package com.demo.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.employeeparking.dto.RequestResponseDto;
import com.example.employeeparking.exceptions.DateNotValid;
import com.example.employeeparking.exceptions.EmployeeNotFoundException;
import com.example.employeeparking.exceptions.EnrollAlreadyDoneException;
import com.example.employeeparking.exceptions.NoFreeSpotsFoundException;
import com.example.employeeparking.exceptions.RequestNotProcessedException;
import com.example.employeeparking.exceptions.SpotRequestNotFoundException;
import com.example.employeeparking.model.Employee;
import com.example.employeeparking.model.FreeSpot;
import com.example.employeeparking.model.SpotRequest;
import com.example.employeeparking.repository.EmployeeRepository;
import com.example.employeeparking.repository.FreeSpotRepository;
import com.example.employeeparking.repository.SpotRequestRepository;


@Service
public class SpotRequestService {
	
	Logger logger = LoggerFactory.getLogger(SpotRequestService.class);
	
	@Autowired
	SpotRequestRepository spotRequestRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;	 
	
	@Autowired
	FreeSpotRepository freeSpotRepository;

	
	public RequestResponseDto raiseRequest(Long employeeId,LocalDate startDate,LocalDate endDate) {
		RequestResponseDto response = new RequestResponseDto();
		Employee employee = employeeRepository.findById(employeeId).orElseThrow(()->new EmployeeNotFoundException("employee not found"));
		LocalDate localDate = LocalDate.now();
		if(employee.getIsVip()==false) {
			if(startDate.compareTo(localDate)>0) {
				int startDate1 = startDate.getDayOfMonth();
				int endDate1 = endDate.getDayOfMonth();
				int difference = endDate1-startDate1;
				if(difference<2) {
					SpotRequest spotRequest1=spotRequestRepository.findSpotRequestByEmployeeAndDate(employee,startDate);
					SpotRequest spotRequest2=spotRequestRepository.findSpotRequestByEmployeeAndDate(employee,endDate);
					if((spotRequest1==null)&&(spotRequest2==null)) {
						for(LocalDate date = startDate;date.compareTo(endDate)<=0;date = date.plusDays(1)) {
							SpotRequest spotRequest = new SpotRequest();
							spotRequest.setEmployee(employee);
							spotRequest.setDate(date);
							spotRequest.setStatus("not assigned");
							spotRequestRepository.save(spotRequest);
						}
						response.setMessage("successfully raised request");
					}
					else {
						throw new EnrollAlreadyDoneException("already raised request");
					}
				}
				else {
					response.setMessage("Raising request is not successfull");
					throw new DateNotValid("you can only request for 2 days");
				}
			}
			else {
				response.setMessage("request is not successfull");
				throw new DateNotValid("start date should be greater than todays date");
			}
		}
		else {
			response.setMessage("request is not successfull");
		}
		return response;
	}
	
	
		public RequestResponseDto requestProcessing() throws RequestNotProcessedException, SpotRequestNotFoundException, NoFreeSpotsFoundException {
			LocalDate date = LocalDate.now();
			List<SpotRequest> employeeReq = spotRequestRepository.findByDateGreaterThanAndStatus(date, "not assigned");
			if(employeeReq.isEmpty()) {
				throw new SpotRequestNotFoundException("no requests found for spots");
			}
			List<Long> spotrequestIds = employeeReq.stream().map(m -> m.getSpotRequesttId()).collect(Collectors.toList());
			Long spotRequestId = getRandomElement(spotrequestIds);
			SpotRequest spotRequest = spotRequestRepository.findById(spotRequestId).get();
			logger.info(" spot request id"+"    "+spotRequestId);

			List<FreeSpot> freeSpots = freeSpotRepository.findByDateGreaterThanAndStatus(date, "not assigned");
			if(freeSpots.isEmpty())
			{
				throw new NoFreeSpotsFoundException("free spots not available");
			}
			List<Long> freeSpotIds = freeSpots.stream().map(m -> m.getFreeSpotId()).collect(Collectors.toList());
			Long freeSpotId = getRandomElement(freeSpotIds);
			FreeSpot freeSpot = freeSpotRepository.findById(freeSpotId).get();
			logger.info(" freeSpot  id"+"    "+freeSpot);
			logger.info("checking the dates");
			if (freeSpot.getDate().equals(spotRequest.getDate())) {
				
				freeSpot.setStatus("assigned");
				spotRequest.setFreeSpot(freeSpot);
				spotRequest.setStatus("assigned");
				freeSpotRepository.save(freeSpot);
				spotRequestRepository.save(spotRequest);

			} else {
				throw new RequestNotProcessedException("request not processed");
			}
			RequestResponseDto responseDto=new RequestResponseDto();
			responseDto.setMessage("requests for parking slot done.....");
			return responseDto;

		}

		public Long getRandomElement(List<Long> list) {
			Random rand = new Random();
			return list.get(rand.nextInt(list.size()));
		}
		
		public List<SpotRequest> getByEmployeeId(long employeeId) throws SpotRequestNotFoundException {
			  Employee employee = employeeRepository.findById(employeeId).orElseThrow(()->new EmployeeNotFoundException("Employee with give id not found"));
				List<SpotRequest> spotRequests = spotRequestRepository.findByEmployee(employee);
				if(spotRequests.isEmpty()) {
					throw new SpotRequestNotFoundException("request not found for spot...");
				}
				else {
					return spotRequests;
				}
				
				
		}
}
