package com.demo.exceptions;

public class ParkingLotNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ParkingLotNotFoundException(String message) {
		super(message);
	}
	
	

}
