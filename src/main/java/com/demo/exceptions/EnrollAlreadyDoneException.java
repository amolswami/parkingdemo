package com.demo.exceptions;

public class EnrollAlreadyDoneException extends RuntimeException {

	public EnrollAlreadyDoneException(String msg) {
		super(msg);

	}
}
