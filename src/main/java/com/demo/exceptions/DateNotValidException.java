package com.demo.exceptions;

public class DateNotValidException extends RuntimeException {

	public DateNotValidException(String msg) {
		super(msg);

	}

}
