package com.demo.exceptions;

public class NoFreeSpotsFoundException extends RuntimeException {

	public NoFreeSpotsFoundException(String message) {
		super(message);
	}

}
