package com.demo.exceptions;

public class RequestNotProcessedException extends RuntimeException {

	public RequestNotProcessedException(String message) {
		super(message);
	}

	

}
