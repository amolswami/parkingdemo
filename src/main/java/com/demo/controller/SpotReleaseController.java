package com.demo.controller;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.employeeparking.dto.RequestResponseDto;
import com.example.employeeparking.service.SpotReleaseService;

/**
 * 
 * @author AMOL
 *
 */
@RestController
@RequestMapping("/responses")
public class SpotReleaseController {
	
	@Autowired
	SpotReleaseService spotReleaseService;
	
	@PostMapping("/")
	public ResponseEntity<RequestResponseDto> releaseSpot(@RequestParam Long employeeId,@RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") LocalDate startDate,
								@RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") LocalDate endDate) {
		RequestResponseDto requestResponseDto = new RequestResponseDto();
		requestResponseDto = spotReleaseService.releaseSpot(employeeId,startDate,endDate);
		return new ResponseEntity<>(requestResponseDto,HttpStatus.OK);
	}

}
