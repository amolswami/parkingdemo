package com.demo.controller;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.employeeparking.dto.RequestResponseDto;
import com.example.employeeparking.exceptions.NoFreeSpotsFoundException;
import com.example.employeeparking.exceptions.RequestNotProcessedException;
import com.example.employeeparking.exceptions.SpotRequestNotFoundException;
import com.example.employeeparking.model.SpotRequest;
import com.example.employeeparking.service.SpotRequestService;
/**
 * 
 * @author AMOL
 *
 */

@RestController
public class SpotRequestController {
	Logger logger = LoggerFactory.getLogger(SpotRequestController.class);
	
	@Autowired
	SpotRequestService spotRequestService;
	
	
	
	@PostMapping("/")
	public ResponseEntity<RequestResponseDto> raiseRequest(@RequestParam Long employeeId,@RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") LocalDate startDate,
								@RequestParam @DateTimeFormat(pattern="dd-MM-yyyy") LocalDate endDate) {
		RequestResponseDto requestResponseDto = new RequestResponseDto();
		requestResponseDto = spotRequestService.raiseRequest(employeeId,startDate,endDate);
		return new ResponseEntity<>(requestResponseDto,HttpStatus.OK);
	}
	
	
	
	@GetMapping("/freespot")
	public ResponseEntity<RequestResponseDto> requestProcessingByLotterySystem() throws RequestNotProcessedException, SpotRequestNotFoundException, NoFreeSpotsFoundException {
		RequestResponseDto message =spotRequestService.requestProcessing();
		logger.info("requestProcessingByLotterySystem in Spot request controller");
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<SpotRequest>> getEmployeeById(@RequestParam long  employeeId) throws SpotRequestNotFoundException {
		List<SpotRequest> EmployeeParkingLot = spotRequestService.getByEmployeeId(employeeId);
		return new ResponseEntity<>(EmployeeParkingLot, HttpStatus.OK);
	}

}
