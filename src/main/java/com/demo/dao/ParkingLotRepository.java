package com.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.employeeparking.model.ParkingLot;

public interface ParkingLotRepository extends JpaRepository<ParkingLot, Long> {

}
