package com.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.employeeparking.model.Employee;
import com.example.employeeparking.model.EmployeeParkingLot;

public interface EmployeeParkingLotRepository extends JpaRepository<EmployeeParkingLot, Long> {

	EmployeeParkingLot findByEmployee(Employee employee);

}
